﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    //public float Hspeed;
    public float speed = 10.0f;
    public float rotateAmount = 0.05f;

    Rigidbody target;
    CursorLockMode mode;

    // Use this for initialization
    void Start()
    {
        target = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        CursorLock();

	 // Hold space for engine propulsion
        if (Input.GetButton("Jump"))
        {
           target.AddForce(transform.forward * speed);
        }

        // values to adjust rotation by comparing current input with previous/mouse(X and Y)
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        
        // float mouseX, mouseY;
        target.AddTorque(0, mouseX * 20.0f,
            mouseY * 20.0f);

        // increase speed when pressing f1
        if (Input.GetKeyDown(KeyCode.F1))
        {
            speed += 50f;
        }
    }


    void Update()
    {
        if(!(Input.GetAxis("Horizontal") > 90) || !(Input.GetAxis("Horizontal") > -90))
        {
            // detect plane banking values to rotate plane by provided amount
            if (Input.GetAxis("Horizontal") < 0) {
                transform.Rotate(rotateAmount, 0, 0);
            }
            else if (Input.GetAxis("Horizontal") > 0)
                transform.Rotate(-rotateAmount, 0, 0);
        }
    }

    void CursorLock()
    {
        // detect mouse button input to lock and unlock cursor
        if (Input.GetMouseButtonDown(0))
        {
            mode = CursorLockMode.Locked;
            Cursor.lockState = mode;
        }
        if (Input.GetMouseButtonDown(1))
        {
            mode = CursorLockMode.None;
            Cursor.lockState = mode;
        }
    }
}
