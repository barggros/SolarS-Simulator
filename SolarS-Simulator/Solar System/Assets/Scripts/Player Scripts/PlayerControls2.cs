﻿using UnityEngine;
using System.Collections;

public class PlayerControls2 : MonoBehaviour
{
    public float hSpeed;

    float speed = 100.0f * 5;
    Rigidbody target;

    // Use this for initialization
    void Start()
    {
        target = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {         
            speed += 50.0f;
        }
        //Add lift force 
        Vector3 lift = Vector3.Project(target.velocity, transform.forward * speed);

        if (Input.GetButton("Jump"))
        {
            target.AddRelativeForce(transform.forward * hSpeed * 2.0f);
        }

        //Banking controls
        target.AddRelativeTorque((-Input.GetAxis("Horizontal") * Time.deltaTime) * transform.forward);

        //Pitch controls
        target.AddRelativeTorque((-Input.GetAxis("Vertical") * Time.deltaTime) * transform.right);

        //
        target.drag = 0.5f * target.velocity.magnitude;
        target.angularDrag = 0.5f * target.velocity.magnitude;
    }
 }

