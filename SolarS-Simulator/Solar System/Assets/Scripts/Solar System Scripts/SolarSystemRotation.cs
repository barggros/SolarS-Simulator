﻿using UnityEngine;
using System.Collections;

public class SolarSystemRotation : MonoBehaviour {

    public float distance;
    public float radSpeed = 0.5f;
    public float rotationSpeed;
    GameObject manager;
    
    // Use this for initialization
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("target2");
        //distance = manager.GetComponent<SphereCollider>().radius - GetComponent<SphereCollider>().radius;
    }


    void Update()
    {
        // rotate main object on its own axis
        transform.Rotate(0, Time.deltaTime * rotationSpeed, 0);

        // rotate around origin (sun)
        transform.RotateAround(manager.GetComponent<Transform>().position,
            Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
