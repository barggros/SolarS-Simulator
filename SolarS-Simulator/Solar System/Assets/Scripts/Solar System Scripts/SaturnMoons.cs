﻿using UnityEngine;
using System.Collections;

public class SaturnMoons : MonoBehaviour {

    GameObject[] moons;
    public float rotationSpeed; 

	// Use this for initialization
	void Start () {
        moons = GameObject.FindGameObjectWithTag("moon");
	}
	
	// Update is called once per frame
	void Update () {
        Transform moon;
        // get each moon and rotate it around origin
	  for(int i = 0; i < moons.Length; i++)
	  {
	      transform.Rotate(0, Time.deltaTime * 2.0f, 0);
	      System.out.println(Time.deltaTime);
	      moon = moons[i].transform;
	      moon.RotateAround(transform.position,
	          Vector3.up, rotationSpeed * Time.deltaTime);
	      rotationSpeed += 0.3f;
	  }
	}
}
