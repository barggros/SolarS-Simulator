﻿using UnityEngine;
using System.Collections;

public class ModifyPlanetSize : MonoBehaviour {

    GameObject[] solarManager;
    GameObject earth, saturn;

	// Use this for initialization
	void Start () {

        // get pointer to Game objects (planets)
        solarManager = GameObject.FindGameObjectsWithTag("planet");
        earth = GameObject.FindGameObjectWithTag("Earth");
        saturn = GameObject.FindGameObjectWithTag("Saturn");

        // create and set transform pointer to objects scale
        Transform eA, saT;
        eA = earth.GetComponent<Transform>();
        saT = saturn.GetComponent<Transform>();
        Vector3 eCurrentSize = eA.localScale;
        Vector3 sCurrentSize = saT.localScale;

        // check size of all planets and increase size dending on planet scale proportions
        for (int i = 0; i < solarManager.Length; i++)
        {
            Vector3 a;
            Transform currentSize = solarManager[i].transform;
            a = currentSize.localScale;
            if (a.x < 0.08f && a.y < 0.08f && a.z < 0.08f)
            {
                a  = a * 1.2f;
                currentSize.localScale = a * 2f;
            } else
            {
                currentSize.localScale = a * 2f;
            }
            
            Debug.Log("Not running");
        }
        eA.localScale = eCurrentSize * 2f;
        saT.localScale = sCurrentSize * 2f;
    }
}
