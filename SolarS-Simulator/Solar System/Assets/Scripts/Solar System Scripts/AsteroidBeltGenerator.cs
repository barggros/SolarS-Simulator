﻿using UnityEngine;
using System.Collections.Generic;

public class AsteroidBeltGenerator : MonoBehaviour
{ 
    //public Transform sun;
    public GameObject ast;

    private Transform manager;
    private static int astAmount = 4000;
    private double axisRotation = 2.5;
    private double rotateSpeed = 10.5;
    HashSet<GameObject> asteroids = new HashSet<GameObject>;

    void Update()
    {
        // populate gameobject array with object amount 
        asteroids = populate(ast, astAmount);

        // detect count per frame (frame counter)
        int i = Time.frameCount;

        // create asteroids live
        if (i < astAmount)
        {
            // randomize variable size for asteroid size
            float size = Random.Range(0.05f, 0.010f);
            Vector3 oSize = new Vector3(size, size, size);

            // select size as new size
            ast.transform.localScale = oSize;

            // create ring around origin by using origins x and z coordinates
            float cx = 2500 * Mathf.Sin(2 * Mathf.PI * i / asteroids.Length);
            float cz = 2500 * Mathf.Cos(2 * Mathf.PI * i / asteroids.Length);

            // randomize spawn (Wall) of asteroids
            cx = cx + Random.Range(50f, 600f);
            float cy = Random.Range(-600f, 600f);
            cz = cz + Random.Range(50f, 600f);

            // create asteroids 
            Instantiate(asteroids[i], new Vector3(cx, cy, cz), Quaternion.identity);
            
            //Debug.Log(" cx" + cx + " " + " cz" + cz);
        }
        
        foreach(GameObject asteroid in asteroids)
        {
            asteroid.transform.RotateAround(Vector.Right * rotateSpeed, 
                                            Vector.Up * axisRotation,
                                            new GameObject().GetComponent( "Sun" ) );
        }
    }

    HashSet<GameObject> populate(GameObject rock, int amount)
    {
        int currentAmount = Time.frameCount;
        
        // populate gameobject array with objects per index i
        HashSet<GameObject> rocks = new HashSet<GameObject>();
        
        do
        {
            rocks.add(new GameObject());
         
        } while(currentAmount < amount);
        
        return rocks;
    }
}
