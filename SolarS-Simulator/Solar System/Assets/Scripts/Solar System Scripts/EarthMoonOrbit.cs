﻿using UnityEngine;
using System.Collections;

public class EarthMoonOrbit : MonoBehaviour {

	public Transform planetA;
    public Vector3 axis = Vector3.up;
    public Vector3 position;
    public float radius;
    public float radSpeed = 0.8f;
    public float rotationSpeed = 15.0f;

    // Use this for initialization
    void Start()
    {
        // get Transform values of object
        planetA = GetComponent<Transform>();
        // set new position
        transform.position = (transform.position - planetA.position).normalized 
            * radius + planetA.position;

        // set radius
        radius = 2.0f;
    }


	void Update ()
    {
        // rotate planet arond origin
        transform.RotateAround(planetA.position, 
            axis, rotationSpeed * Time.deltaTime);
        // set new position
        position = ((transform.position - planetA.position).normalized * 
            radius + planetA.position);
        // rotate around origin
        transform.position = Vector3.MoveTowards(transform.position, 
            position, Time.deltaTime * radSpeed);
	}
}
