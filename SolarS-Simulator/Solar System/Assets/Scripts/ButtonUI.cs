﻿using UnityEngine;
using System.Collections;

public class ButtonUI : MonoBehaviour {

	void OnGUI()
    {
        // create onscreen button & onclick display scene
        if (GUI.Button(new Rect(Screen.width/3,  Screen.height/4, Screen.width/3, Screen.height/4), "Try Space Simmulator"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SolarSystem");
        }
    }
}
