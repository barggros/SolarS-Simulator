﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour
{
    // Camera distance variables
    public Transform target;        //an Object to lock on to  and to control the rotation
    public float minDistance;
    public float maxDistance;
    public float height;
    public float xSpeed = 259.0f;
    public float ySpeed = 120.0f;
    public Transform light;
    public LensFlare flare;
    public float value;

    private Transform camera;
    private bool camButtonPress = false;
    private float xPos, yPos;
    private Camera cam;

    public void Start()
    {
        if (target == null)
        {
            //  Debug.LogWarning("No target Player");
            return;
        }
        else
        {
            camera = transform;
            cam = GetComponent<Camera>();
        }
    }

    // Use this for initialization
    void FixedUpdate()
    {
        // zoom camera on mouse scroll
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            cam.fieldOfView += scroll * 2.0f;
        }

        // set new camera transform values (position) && set old position for the new 
        Vector3 position = new Vector3(target.position.x, target.position.y + height, target.position.z - minDistance);
        camera.position = position;

        // target to focus camera (player)
        camera.LookAt(target);

        // if buttom pressed allow camera view to be set by mouse x and y axis
        if (camButtonPress)
        {
            // set x and y coordinates to calculate rotation
            xPos += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            yPos -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            // set rotation
            Quaternion rotation = Quaternion.Euler(yPos, xPos, 0);
            
            // set camera rotation and position to new values
            camera.rotation = rotation;
            
        } 
        // adjust brightness by distance and set target direction to camera
        CamBrightness();
        target.forward = camera.forward;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
            camButtonPress = true;
        if (Input.GetMouseButtonUp(1))
            camButtonPress = false;

    }

    void CamBrightness()
    {
        Vector3 h = light.position - camera.transform.position;
        float distance = Vector3.Dot(h, camera.forward);
        flare.brightness = h / distance;
    }
}
