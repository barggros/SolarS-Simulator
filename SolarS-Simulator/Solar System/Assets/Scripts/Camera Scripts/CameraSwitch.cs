﻿using UnityEngine;
using System.Collections;

public class CameraSwitch : MonoBehaviour {

    public float rotationSpeed;

    // GameObject playerCam;
    // GameObject farViewCam;
    // GameObject sideCam;
    GameObject camera;
    public bool isActive;

    // Use this for initialization
    void Start () {
        camera = GameObject.FindGameObjectWithTag("MainCamera");
        // farViewCam = GameObject.FindGameObjectWithTag("farViewCam");
        // sideCam = GameObject.FindGameObjectWithTag("SideCam");
	}
	
	void UpdateCamera(string newCam) {
	 switch(newCam) {
	     case "farViewCam": 
	     case "SideCam": 
	     case "MainCamera": GameObject.FindGameObectWithTag(newCam);
	     default: throw new Exception("Invalid Camera");
	 }
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1)) UpdateCamera("MainCamera");
        else if (Input.GetKeyDown(KeyCode.Keypad3)) UpdateCamera("FarViewCam");
        else if (Input.GetKeyDown(KeyCode.Keypad2)) UpdateCamera("SideCam");
        
        
        // Allow farviewcam camera movement to observe the system, movement depends on buttons for 
    	// axis movement (up or down), for free movement both can be pressed simultaneously.
    	if (farViewCam.SetActive(isActive))
        {
    	    // Cursor.lockState = CursorLockMode.Locked;
    	    float Horizontal = Input.GetAxis("Mouse X");
    	    float Vertical = Input.GetAxis("Mouse Y");
    
    	    farViewCam.GetComponent<Transform>().Rotate(Horizontal * rotationSpeed, Vertical * rotationSpeed, 0);
    	    
    	    if(KeyCode.Keypad0) {
    	        cursor.lockState = CursorLockMode.None;    
    	    }
    	    
        } //else Cursor.lockState = CursorLockMode.None;
    }
}

