﻿using UnityEngine;
using System.Collections;

public class FarviewCam : MonoBehaviour {

    public LensFlare flare;
    public float value = 1F;
    public Transform light;
    public float zoomSpeed = 20.0f;
    public float minZoom = 10f;
    public float scrollSpeed = 0.5f;

    private Camera cam;
    private Transform camera;
 
    // Use this for initialization
    void Start () {
        camera = transform;
        cam = GetComponent<Camera>();

        // compare distance of light source position with camera position
        Vector3 h = light.position - camera.position;
        float distance = Vector3.Dot(h, Vector3.right * Time.deltaTime);

        // set camera brightness level based on value by proximity
        flare.brightness = value / distance;
    }
	
	// Update is called once per frame
	void Update () {

        // zoom camera on mouse scroll
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            cam.fieldOfView += scroll * 4.0f;
        }
    }
}
