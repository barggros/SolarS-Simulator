﻿using UnityEngine;
using System.Collections;

public class SideCam : MonoBehaviour
{
    public float height;
    public float distance;

    private float xPos, yPos, xSpeed = 2.0f, ySpeed = 2.0f;
    private Transform sideCam;
    private GameObject target;
    private Transform player;

    private bool cambuttonPress = false;
    private Camera cam;

    // Use this for initialization
    void Start()
    {
        sideCam = transform;
        target = GameObject.FindGameObjectWithTag("Player");
        player = target.transform;
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //detect scrolling wheel in mouse to zoom camera
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            cam.fieldOfView += scroll * 4.0f;
        }

        // get camera current position
        Vector3 camPos = sideCam.position;

        //set new position
        Vector3 newPos = new Vector3(player.position.x,
        player.position.y * height,
        player.position.z - distance);

        // reset camera position
        camPos = newPos;
    }
}
