# Space flight Simulator

-------
-------

The main aspect of the project is designing a space flight-like simulator for those who enjoy watching sci-fi movies such as Star-Trek or Star Wars and would like to fly their own little ship around the Solar System (for Starters), but the simulator is generally aimed at anyone who enjoys the genre “Sci-fi”, as is a very popular genre among young adults, teens and in occasions those in their 30s and 40s who still enjoy the genre. The goal was to make the simulator as realistic as possible with different shots of the player and System for the player(viewer at the same time) to observe the solar system from afar.


The Solar System should contain the necessary planets to rotate around the Sun constantly, including the planets its moon/s (if any) rotating around the planet while the planet rotates on its axis. As a boundary have an Asteroid field which if the player gets close to there would be enough asteroids to block its path and if the player collides with one, get destroyed on impact. An as the main light source the Sun, while making the sun as realistic as possible. The player proximity with a planet or the sun would also destroy the player.


### Requirements & Second Section of Implementation


###### Rotation Cycle and Asteroid Belt:

The steps to follow when creating the scene (Mesh Collider Gravity must be set to false (off)). Where ⋅the Asteroid field (Oort Cloud) would be surrounding the solar system, the moons of each respective planet (in this case Saturn and Earth) rotate around the origin planet (a sphere) and the planets rotate around the origin star/sphere (Sun).


###### Cameras:


Camera 1 and 2 basically the main camera and side camera have as a target the player to set camera position to target, as a child of the target for smoother follow effect. Camera 3 which is the camera for a far view of the solar system (also target).


###### Player Movement and Boundary:


Set the player movements  (banking, pitch and engine propulsion) controls are set to the Mouse X, Y and Jump Axis. For engine propulsion use either KeyCode.Space or  Axis Jump. The boundary is set to the max radius of the asteroid belt, where if the player leaves the belt the player re-spawns inside the belt (not set due to complications).
 

### Assets Requirements

------


(Graphical Assets are not included due to a licencing issue, but the Author name and package name should be enough information to find the assets in the asset store)

###### Sun:

(Must make sure scene directional light is deleted before creating sun as the main light source) 

* First the sun requires the "Unity standard assets" package (effects and Fonts only), for particle effects and more fonts. 
* Then add game objects directional light, pointlight and empty child game object. 
* Set Transform scale proportionally so it's bigger than all other spheres objects in the system, add a helo and a flare, set proportional values of flare according to personal preference, set halo size and  color to personal preference and add texture map in mesh renderer (Texture acquired from google images and not made by me). 
+ Set directional light type to spot, layer to Transparent FX, set range accordingly, prefered color and add desired flare. Empty child object should include:

* Ellipsoid particle emitter, set emission minSize and maxSize accordingly, set Emission accordingly, set rotation to random (untick simulate in world space) and adjust ellipsoid x, y and z to be greater than sphere size.
* Particle Animator set grow size value to 3
* Particle Renderer, add element of your own choosing


###### Space shooter Model:


+ Add rigidbody to model and unset/tick grabity

+ Acquired from unity asset store, package Space Ship Shooter by Duane’s Mind. 

+ Only requirements for player model is to reset size(Transform scale) so as to be smaller than planets, stars, asteroids.


###### Planets:


Set tag to planets under “planet” tag except Saturn and Earth which contain their own tag, then:


* Create spheres around sun and set Transform values accordingly so they are smaller or bigger according to Nasa’s diagram of solar system (or found in google images too) and Add Rigidbodies to each sphere (moons included).
* Add textures in mesh renderer and untick gravity (making object kinematic or not is up to personal preference).
* Assets, models were added and created then placed in folder -> planet textures, textures are acquired from Google free images and not made by me. Basically after adding textures to the mesh renderer materials for each planet according to the texture will be created -> models.
* Add Saturn moons and Earth moon.
* Add Ellipsoid Particle Emitter to Saturn to simulate rings (or so to speak), set rotation to random, untick simulate in world space if its set.
* Add Trail renderer to all planets (spheres) - moons not included.


###### Skybox:


+ Skybox material added from asset store by TL multimedia.

+ The skybox only requires to drag and drop in the scene.


###### Asteroids:


Asteroid material added from unity asset store by Grumpntug.

This model is added to field generator (script) to propagate them accordingly around the solar system.


### Analysis and Design


All scripting done in C#.


#### Camera Scripts:

Also Zooming effect added to all cameras by modifying camera field of view * the scrolling speed based on the Axis “Mouse ScrollWheel”.


###### Script name “LookAtCamera” (attached to object under tag MainCamera)


At start() get the component properties properties necessary and pass them onto the variable declared at scope;


This script adjusts the main camera pointing at the player from behind (in 3rd person), by adjusting the the height and distance to the x and z axis. It checks Update() for value of bool variable set on mouse click to pass onto Fixed update button check to allow the player to move the camera around the player with the mouse cursor. In start() the variables are set to the object component property, to use in other functions.


###### Script name “SideCam” (attached to object under tag SideCam)


At start() get the component properties properties necessary and pass them onto the variable declared at scope; 


This script is to simply adjust the side cam to set the camera position in Update() position to the player position by adjusting height and distance of camera from target player. 


###### Script name “FarviewCam” (attached to object under tag FarViewCamera)


This script adjusts level of brightness to the camera according to distance of the camera from the source(sun flare, light source).


###### Script name “CameraSwitch” (attached to object under tag CamManager)


At start() get the component properties properties necessary and pass them onto the variable declared at scope;


This script allows the player(or viewer) to switch cameras by pressing keypad keys 1,2 and 3 with a series of if statements in function Update(), Also adding movement to FarViewCam (camera tag) by input axis mouse x and mouse y. 



#### Player Scripts:

(attached to object under tag Player) script is optional, PlayerControls script suggested.


###### Script name “PlayerControls”


At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This is the script which handles the player movement controls by receiving input via “Mouse X and Y” axis and engine propulsion to Axis “Jump” by adding torque to the rigidbody of the GameObject for movement on x and y axis and adding force to the object(spaceship) rigidbody for engine propulsion.


###### Scrip name “PlayerControls2”

	
This script is an alternative to the script above where object movement is handled with different controls, basically instead of using Mouse X and Y Axis it uses “Horizontal” for object(spaceship) banking controls and “Vertical” for pitch (nose of the plane basically up or down).	


#### SolarSystem Scripts:


###### Script name “EarthMoonOrgbit”


At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This is a simple script which handles the moons movement around its origin (Earth) by allowing the origin to rotate own its own axis and set the moon rotation and rotation speed accordingly.


###### Script name “SaturnMoons”

	
At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This script handles the rotation of Saturns moons around its origin (Saturn).


###### Script name “SolarManagerRotation”


At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This script manages the rotation of each of the celestial bodies of the solar system around the center/origin (Sun) by calling onto the game object managin the celestial bodies (object must have same coordinates as sun, that includes all axis - x,y and z)


###### Script name “AsteroidBeltGenerator”


At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This script consist of getting the object (asteroid) and populating an array of GameObjects with as many elements as needed with method pupulate() taking a gameobject and an amount to populate the array by, then create a circle by using the sun as the origin and create the ring according to the radius set, the cos which consist of multiplying 2 by pi for a full circumference, again multiplying by i(index i of array length) and sin  basically x and z coordinates. As the script runs on the object Sun, the origin is not required, there all that’s needed is the radius


Instantiating each object and at random size and coordinate by frame count was done on purpose due to performance issues with first and second tryouts.


###### Script name “ModifyPlanetSize”


At start() get the component properties properties necessary and pass them onto the variable declared at scope.


This script was added to avoid having to modify planet sizes manually, therefore modifying the size of the planets accordingly, also checking the smaller spheres and increase the amount by 4 instead of 2 on scene play.


### Implementation


When creating the simulator, creating the sun was one of the hardest parts, so research was required as to what options are available instead of buying a package from the asset store. It took me countless video tutorials as there is a small amount of tutorials for creating solar systems in unity, therefore improvisation was also required. A youtube tutorial helped when adding the “igniting” effect of the sun by using 
particles.


The next step was setting the cameras, first the main camera to the player so we can have a constant 3rd person view of the spaceship also a side camera(works) to add a space like flight effect(or so to speak) to the camera, the third camera was setup just to observe the solar system from above and works as intended. 


After, the next following step was to make the planets rotate around the sun in a more realistic manner or at least the more realistic the better. This was achieved by placing the suns in the right coordinates manually (a simple algorithm could have been implemented for this but main camera and player controls were still troublesome and still are but work more or less but not the way It was envisioned).


The field generator Algorithm was achievable by researching algorithms and reading articles of how to make a circle, there were many unsuccessful tryouts as C# is a the unity engine functions/methods are still relatively new to me as a developer. But in the end it was achievable with some math.


Resizing the planets was a matter of adjusting the scales of each planet on one script (due to laziness).   


### Evaluation 


#### What was achieved:


Solar system with:


* Planet rotation
* Moon rotation
* Adding controls to the player
* Adding zooming effect to cameras
* Setting cameras properly (not MainCamera)
* Asteroid belt as a boundary
* Creating a sun
* Different camera views
* Camera Switch controls


#### What wasn’t achieved (will be solved in the coming weeks/months due to other projects):


* Proper controls for the player
* MainCamera adjusted accordingly
* Not fully finished Oort Cloud to surround the Solar System(Asteroid Belt)
* No boundary (limit x,y,z coordinates)set properly for the player
* No respawn set if player hits planets/star or asteroids


#### Problem/s encountered (Will be solved In the following weeks):


The main problem encountered is adjusting the controls of plane and adjusting the camera accordingly, set to the player’s view so if the player moves the camera moves with him and like so in return, basically simple smooth effect when the player is moving. The plane controls seemed to be problematic as the sensitivity of the mouse affects the Mouse X and Y axis, therefore if the plane anks right or left or goes up or down it would not stop spinning continuously.


